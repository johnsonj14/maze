import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * An efficient Disjoint Set class that implements a maze generator in the main
 */
public class DisjointSet
{
   private static final int ROOT = -1;
   private int[] _unionTable;

   /**
    * The constructor class for the DisjointSet. Initializes an array of size
    * with values of -1.
    * 
    * @param size is the size of the disjointSet
    */
   public DisjointSet(int size)
   {
      _unionTable = new int[size];
      for (int i = 0; i < _unionTable.length; i++)
      {
         _unionTable[i] = ROOT;
      }
   }

   /**
    * Union two disjoint sets using the height heuristic. union-by-height.
    * 
    * @param p the root of set 1.
    * @param q the root of set 2.
    */
   public void union(int p, int q)
   {
      // q is deeper, make q new root
      if (_unionTable[q] < _unionTable[p])
      {
         _unionTable[p] = q;
      }
      else
      {
         // Update height if they are the same height, make p new root
         if (_unionTable[p] == _unionTable[q])
         {
            _unionTable[p]--;
         }
         _unionTable[q] = p;
      }
   }

   /**
    * Perform a find with path compression.
    * 
    * @param x the element being searched for.
    * @return the set containing x.
    */
   public int find(int p)
   {
      int set;
      if (_unionTable[p] <= ROOT)
      {
         set = p;
      }
      else
      {
         // compresses the path when finding it
         set = _unionTable[p] = find(_unionTable[p]);
      }
      return set;
   }

   /**
    * Are p and q connected
    * 
    * @param p is one place in the set
    * @param q is the another place in the set
    * @return whether p and q are connected
    */
   public boolean isConnected(int p, int q)
   {
      return find(p) == find(q);
   }
   
   /**
    * Generates a maze using the DisjointedSet class
    * 
    * @param args is N of the NxN maze
    */
   public static void main(String[] args)
   {
      final int MIN_ARGS = 1;
      int size = 0;
      DisjointSet djset;
      Map<Integer, ArrayList<Integer>> destroyedWalls = new HashMap<Integer, ArrayList<Integer>>();
      ArrayList<Integer> emptyList = new ArrayList<Integer>();

      // Make sure we are given one arg that is a positive integer
      //    Throw error and exit if there is an error.
      if (args.length < MIN_ARGS)
      {
         System.err.println("Insufficient Arguments");
         System.exit(1);
      }
      try
      {
         size = Integer.parseInt(args[0]);
         if (size < 1)
         {
            throw new UnsupportedOperationException();
         }
      }
      catch (NumberFormatException | UnsupportedOperationException e)
      {
         System.err.println("Supposed to be a positive integer!");
         System.exit(1);
      }

      // Since the size has been verified as valid, make an array of
      //    valid neighboring positions and make the disjoint set.
      int[] mazeNeighborPositions = {-size, -1, 1, size};
      djset = new DisjointSet(size * size);
      final int ENTRANCE = 0;
      final int EXIT = (size * size) - 1;

      // Variables to be used when knocking down random walls in the
      //    maze.
      boolean isComplete = false;
      Random rand = new Random();
      int randomPosition;
      int randomNeighbor;
      
      // Union two random neighboring positions in the disjointedSet 
      //    until the maze is connected.
      while (!isComplete)
      {
         // Choose a random position and choose a random neighbor of that position
         randomPosition = rand.nextInt(size * size);
         randomNeighbor = randomPosition + mazeNeighborPositions[rand.nextInt(4)];
         
         // Special cases for when an edge position is randomly selected. Make 
         //    sure to randomly pick a neighbor that is actually touching.
         // Left border column
         if (randomPosition % size == 0)
         {
            if ((randomNeighbor == randomPosition - 1) || (randomNeighbor < 0) 
                  || (randomNeighbor >= (size * size)))
            {
               randomNeighbor = randomPosition + 1;
            }
         }
         // Right border column
         if (randomPosition % size == (size - 1))
         {
            if ((randomNeighbor == randomPosition + 1) || (randomNeighbor < 0) 
                  || (randomNeighbor >= (size * size)))
            {
               randomNeighbor = randomPosition - 1;
            }
         }
         // Top border row
         if (randomPosition <= size) 
         {
            if (randomNeighbor < 0) 
            {
               randomNeighbor = randomPosition + size;
            }
         }
         // Bottom border row
         if (randomPosition > (size * (size -1))) 
         {
            if (randomNeighbor >= (size * size))
            {
               randomNeighbor = randomPosition - size;
            }
         }
         
         // Union the randomly selected positions, only if they are not connected already.
         if (!djset.isConnected(randomPosition, randomNeighbor))
         {
            // Keep track of which wall was destroyed.
            destroyedWalls.putIfAbsent(randomPosition, new ArrayList<Integer>());
            destroyedWalls.get(randomPosition).add(randomNeighbor);
            // Union positions.
            djset.union(djset.find(randomPosition), djset.find(randomNeighbor));
            // Maze is complete if the entrance is connected to the exit.
            if (djset.isConnected(djset.find(ENTRANCE), djset.find(EXIT)))
            {
                  isComplete = true;
            }
         }         
      }

      // Print out the maze.
      // sideIndex keeps track of the position when comparing horizontally. 
      int sideIndex = 0;
      // underIndex keeps track of the position when comparing vertically.
      int underIndex = 0;
      System.out.print("   ");
      // Print the top row of the maze, leaving a slit in the top-left
      //    for the entrance.
      for (int i = 0; i < size - 1; i++)
      {
         System.out.print("---");
      }
      System.out.println("");
      // For every row print out the horizontal and lower walls.
      for (int i = 0; i < size; i++)
      {
         System.out.print("|");
         // For neighboring horizontal positions, print based on if a wall exists.
         for (int j = 0; j < size - 1; j++)
         {
            System.out.print("  ");
            // if there is a destroyed wall, print empty whitespace. Else,
            //    print the wall.
            if (destroyedWalls.getOrDefault(sideIndex, emptyList).contains(sideIndex + 1) || 
                  destroyedWalls.getOrDefault(sideIndex + 1, emptyList).contains(sideIndex))
            {
               System.out.print(" ");
            }
            else
            {
               System.out.print("|");
            }
            sideIndex++;
         }
         System.out.print("  ");
         sideIndex++;
         System.out.println("|");

         // if the maze hasn't already reached the bottom row
         if (sideIndex < (size * size))
         {
            // For neighboring vertical positions, print based on if a wall exists.
            for (int j = 0; j < size; j++)
            {
               // if there is a destroyed wall, print empty whitespace. Else,
               //    print the wall.
               if (destroyedWalls.getOrDefault(underIndex, emptyList).contains(underIndex + size) ||
                     destroyedWalls.getOrDefault(underIndex + size, emptyList).contains(underIndex))
               {
                  System.out.print("   ");
               }
               else
               {
                  System.out.print(" --");
               }
               underIndex++;
            }
            System.out.println("");
         }
      }
      // Print the bottom border.
      for (int i = 0; i < size - 1; i++)
      {
         System.out.print("---");
      }
   } // main
} // DisjointSet
